package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private final int rows;
    private final int cols;
    private final CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];

    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    /**
     * Method that checks if the cell is "out of bounds" of the grid. If so, it returns false,
     * Method that checks if a cell is "out of bounds" of the grid. If so, it returns false,
     * else it returns true. I made this a method as it is used in two other methods.
     */

    public boolean checkIndexValue(int row, int column) {
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }


    @Override
    public void set(int row, int column, CellState element) {
        if (checkIndexValue(row, column)) {
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (checkIndexValue(row, column)) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(numRows(), numColumns(), CellState.DEAD);

        for (int row = 0; row < numRows(); row++) {
            for (int column = 0; column < numColumns(); column++) {
                gridCopy.set(row, column, get(row, column));
            }
        }
        return gridCopy;
    }

}
